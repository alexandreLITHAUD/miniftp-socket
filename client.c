/******************************************************************************/
/*			Application: ...					*/
/******************************************************************************/
/*									      */
/*			 programme  CLIENT				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs : ... 					*/
/*									      */
/******************************************************************************/	


#include <stdio.h>
#include <curses.h> 		/* Primitives de gestion d'ecran */
#include <sys/signal.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h> // A VOIR

#include "fon.h"   		/* primitives de la boite a outils */

#define SERVICE_DEFAUT "1111"
#define SERVEUR_DEFAUT "127.0.0.1"

#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

#define DATAPATH "./clientData/"

int putFileToServerQuery(char* fileName, int numSocket);
char* convertLenght(int lenght);
void connectClientServer(int *socket, char *serveur, char *service);
void client_appli (char *serveur, char *service);
void getFileFromServerQuery(char* fileName, int numSocket);
void readData(int numSocket, char* buffer, int *tailleFichier, int *tailleNomFichier);


/*****************************************************************************/
/*--------------- programme client -----------------------*/

int main(int argc, char *argv[])
{

	char *serveur= SERVEUR_DEFAUT; /* serveur par defaut */
	char *service= SERVICE_DEFAUT; /* numero de service par defaut (no de port) */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch(argc)
	{
 	case 1 :		/* arguments par defaut */
		  printf("serveur par defaut: %s\n",serveur);
		  printf("service par defaut: %s\n",service);
		  break;
  	case 2 :		/* serveur renseigne  */
		  serveur=argv[1];
		  printf("service par defaut: %s\n",service);
		  break;
  	case 3 :		/* serveur, service renseignes */
		  serveur=argv[1];
		  service=argv[2];
		  break;
    default:
		  printf("Usage:client serveur(nom ou @IP)  service (nom ou port) \n");
		  exit(1);
	}

	/* serveur est le nom (ou l'adresse IP) auquel le client va acceder */
	/* service le numero de port sur le serveur correspondant au  */
	/* service desire par le client */
	
	client_appli(serveur,service);
}

/*****************************************************************************/
void client_appli (char *serveur,char *service)

/* procedure correspondant au traitement du client de votre application */

{
	int numSocket;
	int running = 1;
	char operation[20];

	char fileName[100];

	int tailleNomFichier = 0;
	int tailleFichier = 0;

	connectClientServer(&numSocket, serveur, service);

	while(running){

		char* buffer = malloc(2000*sizeof(char));
		printf(YELLOW"What do you want to do ? (20 character max)\n"NOCOLOR);
		fflush(stdin);
		scanf("%19s", operation);

		if(!strcmp(operation,"put")){

			printf(YELLOW"What file do you want to be send ? (99 character max)\n"NOCOLOR);
			scanf("%99s", fileName);

			
			int verif = putFileToServerQuery(fileName,numSocket);
			if(verif == EXIT_FAILURE){
				printf(RED"Command Abborted..\n"NOCOLOR);
			}
		}

		else if(!strcmp(operation,"get")){
			
			printf(YELLOW"What file do you want ? (99 character max)\n"NOCOLOR);
			scanf("%99s", fileName);
			getFileFromServerQuery(fileName, numSocket);

			h_reads(numSocket,buffer,1);
			int verif = atoi(buffer);

			if(verif == EXIT_FAILURE){
				printf(RED"Command Abborted..\n"NOCOLOR);
			}
			else{
				readData(numSocket, buffer, &tailleFichier, & tailleNomFichier);
			}
		}

		else if(!strcmp(operation,"ls")){

			h_writes(numSocket, "3",1);

			h_reads(numSocket,buffer,1);
			int verif = atoi(buffer);
			readData(numSocket, buffer, &tailleFichier, & tailleNomFichier);
		}

		else if(!strcmp(operation,"exit")){
			running = 0;
			h_writes(numSocket,"4",1);
			h_close(numSocket);
		}

		else{
			printf(RED"Command not found...\n"NOCOLOR);
			printf(BLUE"Function recognised : \n"NOCOLOR);
			printf(BLUE"Add a file -> put then in the other box send the file name\n"NOCOLOR);
			printf(BLUE"Get a file -> get then in the other box send the file name\n"NOCOLOR);
			printf(BLUE"Print all the file in server -> ls\n"NOCOLOR);
			printf(BLUE"Exit the commande prompt -> exit\n"NOCOLOR);
		}

		free(buffer);

	}

	exit(EXIT_SUCCESS);

}

/*****************************************************************************/

void connectClientServer(int *socket, char *serveur, char *service){

	int numSocket = h_socket(AF_INET, SOCK_STREAM);
	*socket = numSocket;

	struct sockaddr_in *serverIP;

	adr_socket(service, serveur, SOCK_STREAM, &serverIP);

	h_connect(numSocket, serverIP);
}
/*****************************************************************************/

int putFileToServerQuery(char* fileName, int numSocket){
	FILE* ptr;
	char ch;
	int lenght = 0;
	int index = 0;
	int i = 0;
	int fileNameLength = 0;

	char pathFile[50];
	sprintf(pathFile,"%s%s",DATAPATH,fileName);

	ptr = fopen(pathFile,"r");
	if(ptr == NULL){
		printf(RED"File cannot be oppened\n"NOCOLOR);
		return EXIT_FAILURE;
	}

	do {
        ch = fgetc(ptr);
		lenght++;
    } while (ch != EOF);
	lenght++;

	while(fileName[i] != '\0'){
		fileNameLength++;
		i++;
	}

	char* temp = malloc(lenght+fileNameLength+4+4+1 * sizeof(char));

	char* trueFileNameLenght = convertLenght(fileNameLength);
	char* paquetLenght = convertLenght(lenght);

	temp[0] = '1';

	temp[1] = trueFileNameLenght[0];
	temp[2] = trueFileNameLenght[1];
	temp[3] = trueFileNameLenght[2];
	temp[4] = trueFileNameLenght[3];

	temp[5] = paquetLenght[0];
	temp[6] = paquetLenght[1];
	temp[7] = paquetLenght[2];
	temp[8] = paquetLenght[3];

	index = 9;

	fseek(ptr, 0, SEEK_SET);

	for(int j = 0;j<fileNameLength;j++){
		temp[index+j] = fileName[j];
	}

	index += fileNameLength;

	do {
        ch = fgetc(ptr);
		temp[index] = ch;
		index++;
    } while (ch != EOF);

	h_writes(numSocket, temp, lenght+fileNameLength+4+4+1);

	free(paquetLenght);
	free(temp);

	fclose(ptr);

	return EXIT_SUCCESS;
}


/*****************************************************************************/

char* convertLenght(int lenght){

	char* res = malloc(5 * sizeof(char));

	sprintf(res,"%d",lenght);
	
	res[4] = '\0';

	return res;
}
/*****************************************************************************/

void getFileFromServerQuery(char* fileName, int numSocket){
	int index = 0;
	int fileNameLength = 0;
	int i = 0;

	while(fileName[i] != '\0'){
		fileNameLength++;
		i++;
	}

	char* temp = malloc(fileNameLength+4+1 * sizeof(char));

	char* trueFileNameLenght = convertLenght(fileNameLength);

	temp[0] = '2';

	temp[1] = trueFileNameLenght[0];
	temp[2] = trueFileNameLenght[1];
	temp[3] = trueFileNameLenght[2];
	temp[4] = trueFileNameLenght[3];

	index = 5;

	for(int j = 0;j<fileNameLength;j++){
		temp[index+j] = fileName[j];
	}

	index += fileNameLength;

	h_writes(numSocket, temp,fileNameLength+4+1);
	free(temp);
}

void readData(int numSocket, char* buffer, int *tailleFichier, int *tailleNomFichier){
    // READ FILE NAME LENGHT
    h_reads(numSocket, buffer, 4);
    *tailleNomFichier = atoi(buffer);

    // READ FILE CONTENT LENGHT
    h_reads(numSocket, buffer, 4);
    *tailleFichier = atoi(buffer);

    // READ FILE NAME
    h_reads(numSocket, buffer, *tailleNomFichier);

    // READ FILE CONTENT
    h_reads(numSocket, buffer, *tailleFichier);
    printf(GREEN"%s\n"NOCOLOR,buffer);
}