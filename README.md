
# Mini FTP Socket : Lithaud Puech Guyot

- [Mini FTP Socket : Lithaud Puech Guyot](#mini-ftp-socket--lithaud-puech-guyot)
- [Manuel d'utilisation](#manuel-dutilisation)
- [Cahier des charges](#cahier-des-charges)
  - [Minimal](#minimal)
  - [Add-ons](#add-ons)
- [Choix du mode de communication](#choix-du-mode-de-communication)
- [Algorithmes](#algorithmes)
  - [Client :](#client-)
  - [Serveur :](#serveur-)
- [Problèmes à gérer](#problèmes-à-gérer)
  - [Comment réaliser la fonction ls ?](#comment-réaliser-la-fonction-ls-)
  - [Comment envoyer/recevoir un fichier ?](#comment-envoyerrecevoir-un-fichier-)
- [Problèmes rencontrés](#problèmes-rencontrés)

# Manuel d'utilisation

Ce programme possède 4 fonctions :

  - **put** -> pour l'utiliser il faut d'abord faire put , **faire un retour chariot** puis tapper le nom du fichier a put 
  - **get** -> pour l'utiliser il faut d'abord faire get , **faire un retour chariot** puis tapper le nom du fichier a get 
  - **ls** -> affiche les fichier présent dans le serveur
  - **exit** -> quitte le programme et coupe les communications 

# Cahier des charges

## Minimal

- Plusieurs fichiers sont disponibles sur un serveur.
- Un client une fois connecté à ce serveur, peut effectuer les commandes suivantes :
    - ls : affichage de la liste des fichiers disponibles sur le serveur.
    - get nom-fichier : envoie du serveur au client du fichier de nom nom-fichier.
    - put nom-fichier : envoie du client au serveur du fichier de nom nom-fichier.

## Add-ons

- Un client une fois connecté à ce serveur, peut en plus effectuer les commandes suivantes :
    - // cat: affichage du contenu d’un fichier
    - rm: suppression d’un fichier
- Gestion de plusieurs connexions clients en parallèle

# Choix du mode de communication

>Pour notre application miniFTP, nous utiliserons le mode de communication TCP.

En effet, bien qu’UDP soit adapté à aux communications “question-réponse”, le transfert d’un fichier représente un échange conséquent de données. Ainsi, la récupération d'erreurs, la gestion de flux sont des fonctionnalités indispensables. De plus, comme les fichiers peuvent être lourds il est important d’avoir TPC car UDP ne retournera pas forcément les paquets dans l’ordre.

De plus, le transfert de fichiers lourds sollicitent longuement le serveur ce qui entraîne beaucoup d’attente. Ainsi, nous créerons une version prototype en itératif puis une version finale en parallèle.



# Algorithmes

## Client :

- Etablir une connexion avec le serveur
- Créer un tableau de caractères appelé “entrée utilisateur”
- Créer un pointeur de caractère appelé “nom de fichier”
-Créer un caractère “choix utilisateur”
- Créer un entier appelé “réponse”
- Tant que “entrée utilisateur” != “quit” :
    - Si “entrée utilisateur” == “ls”:
      - Faire le ls
    - Sinon, si “entrée utilisateur”[:3] == “put”:
        - Parser “entrée utilisateur” pour récupérer le nom de fichier via sscanf et le stocker dans “nom de fichier”
        - Demander au serveur de créer un fichier du même nom et attendre sa réponse qu’on stockera dans “réponse”...
        - Si “réponse” vaut “ok c’est créé” :
            - Envoyer les données **(1)**
        - Sinon, si “réponse” vaut “fichier déjà existant”:
            - Demander à l’utilisateur si il souhaite écraser le fichier existant [y] ou annuler l’opération [any] et stocker le résultat dans “choix utilisateur”.
            - Si “choix utilisateur” == ‘y’:
                - Envoyer les données **(1)**
            - Sinon :
                - printf(“Opération annulée\n”);
        - Sinon:
            - printf(“Impossible d’effectuer la demande\n”);
    - Sinon, si “entrée utilisateur”[:3] == “get”:
        - Parser “entrée utilisateur” pour récupérer le nom de fichier via sscanf et le stocker dans “nom de fichier”
        - Demander au serveur si le fichier “nom de fichier” existe et attendre sa réponse qu’on stockera dans “réponse” …
        - Si (“réponse” == “il existe” et “nom de fichier” n’existe pas en local):
            - fichier_get = fopen(nom_de_fichier, “w”);
            - Demander au serveur d’envoyer les données qu’on stockera au fur et à mesure dans le fichier “nom de fichier” **(1)**
            - close(nom_de_fichier);
        - Sinon, si (“réponse” == “il existe” et “nom de fichier” existe en local):
            - Demander à l’utilisateur si il souhaite écraser le fichier existant [y] ou annuler l’opération [any] et stocker le résultat dans “choix utilisateru”.
            - Si “choix utilisateur” == ‘y’:
                - fichier_get = fopen(nom_de_fichier, “w”);
                - Demander au serveur d’envoyer les données qu’on stockera au fur et à mesure dans le fichier “nom de fichier” **(1)**
                - close(nom_de_fichier);
            - Sinon:
                - printf(“Opération annulée\n”);
        - Sinon:
            - printf(“Impossible d’effectuer la demande\n”);
    - Sinon:
        - printf(“Commande introuvable !\nCommandes disponibles : put, get, ls\n”);

___

- **(1)**: Lorsqu’il y a un échange de données, on procède de la manière suivante :
    - **(a)** L’émetteur envoie un paquet avec la taille et si il s’agît du dernier paquet
    - **(b)** Le récepteur répond soit “ok” soit “impossible”
    - **(c)** Si “ok”, l’émetteur envoie le paquet de la taille, sinon break
    - **(d)** Répéter jusqu’à ce qu’il s’agisse du dernier paquet
___

## Serveur :

- Main : 
    - Mettre le serveur en écoute
    - Faire:
        - S’il y a une demande de connexion en attente:
            - accepter la connexion
            - Tant que la connexion n’est pas fermée côté client:
                - Si commande reçu:
                    - Traiter la demande<br /><br />
- Traiter la demande ls :
    - Lancer commande système ls redirigé vers .output
    - Calculer taille de .output
    - Envoie de la taille
    - Passer .output en string
    - envoi de la string<br /><br />
- Traiter la demande get :
    - Lire la taille du nom de fichier (4 premiers octets)
    - Lire le nom de fichier
    - Tester la présence du fichier et calculer la taille du fichier
    - Créer localement le fichier avec la taille du fichier et la taille du nom du fichier<br /><br />
- Traiter la demande put :
    - Lire la taille du nom de fichier
	- Lire la taille des données du fichier
	- Lire le nom du fichier
	- Lire les données du fichier<br /><br />
- serv_ls :
    - On utilise la commande "system" de <stdlib.h> qui permet d'appeler une fonction bash dans un programme, en particulier ``ls`` dans notre cas.

# Problèmes à gérer

## Comment réaliser la fonction ls ?

>Pour réaliser la commande ls nous avons eu l’idée de réaliser cette même commande dans le serveur avec une redirection dans un fichier (qui aura un nom spécifique introuvable par la fonction ls comme .output par exemple) ( ls > .output ) puis ensuite l’envoyer comme un fichier classique.

>De plus comme le serveur sera un Linux il serait possible théoriquement de pouvoir avoir un client fonctionnant sur n’importe quel système d’exploitation (même ceux qui n’on pas de ls comme Windows par exemple)

## Comment envoyer/recevoir un fichier ?

>Pour ce faire nous comptons envoyer 2 trames principales. Un avec la taille du fichier qui va être envoyé. Et l’autre avec l’intégralité du fichier (de taille envoyé juste avant) qui peut faire plus que 1 paquet si le fichier est grand. Comme nous utilisons TCP pour les raisons vu précédemment nous serons assurés d’avoir l’intégralité du texte et dans le bon ordre.

# Problèmes rencontrés

Lors de la finalisation du programme nous avons rencontré un problème que nous n'avons malheureusement pas réussi à résoudre.

Quand nous avons essayer de transformer les données envoyé du put dans un fichier nous avons rencontré des erreurs très étranges avec les malloc. Leur simple présence empêchai la connexion des sockets (il nous disai impossible de ce connecter au socket -1). Nous avons donc mis de coté cette étape afin d'avoir un code fonctionnel même si les partages ne se font que par l'intermédaire de la console.

Après un certain nombre de tests effectué nous pensont que cette erreur viens d'une trop grande présence de malloc sans le code.

$$\int_{0}^{1} |ln(x)|dx = 1$$