/******************************************************************************/
/*			Application: ....			              */
/******************************************************************************/
/*									      */
/*			 programme  SERVEUR 				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs :  ....						      */
/*		Date :  ....						      */
/*									      */
/******************************************************************************/	

#include<stdio.h>
#include <curses.h>

#include<sys/signal.h>
#include<sys/wait.h>
#include<stdlib.h>

#include "fon.h"     		/* Primitives de la boite a outils */

#define SERVICE_DEFAUT "1111"

#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

#define DATAPATH "./serverData/"
#define OUTPUTFILE ".output"

void serveur_appli (char *service);   /* programme serveur */
void connectServerClient(char *service, int *socket, struct sockaddr_in *serveurIP);
int putFileToClient(char* fileName, int numSocket);
char* convertLenght(int lenght);
void sendLS(int newNumSocket);

/******************************************************************************/	
/*---------------- programme serveur ------------------------------*/

int main(int argc,char *argv[])
{

	char *service= SERVICE_DEFAUT; /* numero de service par defaut */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc)
 	{
   	case 1:
		  printf("defaut service = %s\n", service);
		  		  break;
 	case 2:
		  service=argv[1];
            break;

   	default :
		  printf("Usage:serveur service (nom ou port) \n");
		  exit(1);
 	}

	/* service est le service (ou numero de port) auquel sera affecte
	ce serveur*/
	
	serveur_appli(service);
}


/******************************************************************************/	
void serveur_appli(char *service)

/* Procedure correspondant au traitemnt du serveur de votre application */

{

	/* Faire une fonction */
	int numSocket;
	struct sockaddr_in *serveurIP;
	connectServerClient(service,&numSocket,serveurIP);

	int newNumSocket = h_accept(numSocket, serveurIP);
		printf("=========================================\n");
		printf("======== DEBUT CONNEXION CLIENT =========\n");
		printf("=========================================\n");

	while(1){

		char* buffer = malloc(2000*sizeof(char));
		int tailleFichier = 0;
		int tailleNomFichier = 0;

		// READ QUERY
		h_reads(newNumSocket, buffer, 1);
		int query = atoi(buffer);

		switch (query)
		{
		case 1:
			//READ AND PUT IN REPOSITORY
			printf(YELLOW"Traitement de requête PUT\n"NOCOLOR);

			// READ FILE NAME LENGHT
			h_reads(newNumSocket, buffer, 4);
			tailleNomFichier = atoi(buffer);

			// READ FILE CONTENT LENGHT
			h_reads(newNumSocket, buffer, 4);
			tailleFichier = atoi(buffer);

			// READ FILE NAME
			h_reads(newNumSocket, buffer, tailleNomFichier);

			// READ FILE CONTENT
			h_reads(newNumSocket, buffer, tailleFichier);
			printf(GREEN"%s\n"NOCOLOR,buffer);

			break;
		
		case 2:
			printf(YELLOW"Traitement de requête GET\n"NOCOLOR);
			//RETURN THE REQUESTED FILE

			// READ FILE NAME LENGHT
			h_reads(newNumSocket, buffer, 4);
			tailleNomFichier = atoi(buffer);

			// READ FILE NAME
			h_reads(newNumSocket, buffer, tailleNomFichier);

			
			int verif = putFileToClient(buffer,newNumSocket);

			break;

		case 3:
			printf(YELLOW"Traitement de requête LS\n"NOCOLOR);
			sendLS(newNumSocket);

			break;

		case 4:
			h_close(newNumSocket);
			exit(0);

		default:
			printf(RED"Should not happend...\n"NOCOLOR);
			exit(1);
		}

		free(buffer);
	}

}

/******************************************************************************/	
void connectServerClient(char *service, int *socket, struct sockaddr_in *serveurIP){

	int numSocket = h_socket(AF_INET, SOCK_STREAM);
	*socket = numSocket;

	adr_socket(service, NULL, SOCK_STREAM, &serveurIP);

	h_bind(numSocket, serveurIP);

	h_listen(numSocket, 5);

}
/*****************************************************************************/

int putFileToClient(char* fileName, int numSocket){
	FILE* ptr;
	char ch;
	int lenght = 0;
	int index = 0;
	int i = 0;
	int fileNameLength = 0;

	char pathFile[50];
	sprintf(pathFile,"%s%s",DATAPATH,fileName);

	ptr = fopen(pathFile,"r");
	if(ptr == NULL){
		printf(RED"File cannot be oppened\n"NOCOLOR);
		h_writes(numSocket,"1",1);
		return EXIT_FAILURE;
	}

	do {
        ch = fgetc(ptr);
		lenght++;
    } while (ch != EOF);
	lenght++;

	while(fileName[i] != '\0'){
		fileNameLength++;
		i++;
	}

	char* temp = malloc(lenght+fileNameLength+4+4 * sizeof(char));

	char* trueFileNameLenght = convertLenght(fileNameLength);
	char* paquetLenght = convertLenght(lenght);

	temp[0] = trueFileNameLenght[0];
	temp[1] = trueFileNameLenght[1];
	temp[2] = trueFileNameLenght[2];
	temp[3] = trueFileNameLenght[3];

	temp[4] = paquetLenght[0];
	temp[5] = paquetLenght[1];
	temp[6] = paquetLenght[2];
	temp[7] = paquetLenght[3];

	index = 8;

	fseek(ptr, 0, SEEK_SET);

	for(int j = 0;j<fileNameLength;j++){
		temp[index+j] = fileName[j];
	}

	index += fileNameLength;

	do {
        ch = fgetc(ptr);
		temp[index] = ch;
		index++;
    } while (ch != EOF);

	h_writes(numSocket,"0",1);
	h_writes(numSocket, temp, lenght+fileNameLength+4+4);

	free(paquetLenght);
	free(temp);

	fclose(ptr);

	return EXIT_SUCCESS;
}


/*****************************************************************************/

char* convertLenght(int lenght){

	char* res = malloc(5 * sizeof(char));

	sprintf(res,"%d",lenght);
	
	res[4] = '\0';

	return res;
}

/*****************************************************************************/

void sendLS(int newNumSocket) {
	system("ls ./serverData/ > ./serverData/.output");

	putFileToClient(".output", newNumSocket);
}

